#include <stdio.h>
#include <string.h>
#include <curl/curl.h>

void post(const char *postthis, char * url) {
  CURL *curl;
  CURLcode res;
 

  curl = curl_easy_init();
  if(curl) {

    struct curl_slist *chunk = NULL;
 
    chunk = curl_slist_append(chunk, "Content-type: application/json");
 
    /* set our custom set of headers */ 
    res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);


    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);
 
    /* if we don't provide POSTFIELDSIZE, libcurl will strlen() by
       itself */ 
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
 
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);

    curl_slist_free_all(chunk);
  }
}

