#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "http.h"
#include "serializer.h"

typedef struct rgb {
    int r;
    int g;
    int b;
} rgb;

int main(void)
{

  char *url = "http://localhost:3000/api/color";
  rgb color = {100,200,30};
  const char* body = json(color.r, color.g, color.b);
  post(body, url);
  sleep(2);

  return 0;
}

