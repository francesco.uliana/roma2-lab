#include <json.h>

const char* json(int r, int g, int b) {
    json_object * a = json_object_new_object();
    json_object_object_add(a, "r", json_object_new_int(r));
    json_object_object_add(a, "g", json_object_new_int(g));
    json_object_object_add(a, "b", json_object_new_int(b));
    const char* body = json_object_get_string(a);

    return body;
}